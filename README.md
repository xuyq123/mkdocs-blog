# README

## 简介

九零后，程序员，热爱生活、乐于记录的普通青年。

---

> 关于编程

**技术还可以的程序猿。**

会 `java`  `linux`  `mysql` `redis` ...  <br/>
用 `git` `idea` `postman` `markdown` ...  <br/>

| 序号  | 平台    | 地址        |  备注          |
| ----- | -----   | ----------- |  ------------- |
| 1     | gitlab  | [MyNotes]( https://gitlab.com/xuyq123/mynotes )                  | 编程笔记   |
| 2     | github  | [vuepress-blog]( https://github.com/scott180/vuepress-blog )  	 | 博客网站   |
| 3     | gitcode | [document]( https://gitcode.net/xu180/document )  				 | 生活随笔   |

---

<br/>

## 网站

> 博客网站

**生活随笔-编程笔记-书法练习轨迹。**

| 徐书法 | 地址        |  备注          |
| -----  | ----------- |  ------------- |
| 1      | [xushufa]( https://xushufa.cn )                  	    | `vuepress-theme-reco`构建的博客网站。|
| 2      | [vuepress-blog]( https://vuepress-blog.xushufa.cn )  	| `vuepress`构建的博客网站。           |
| 3      | [mkdocs-blog]( https://xuyq123.gitlab.io/mkdocs-blog )   | `mkdocs`构建的博客网站。             |

---

> 个人邮箱 

1021151991@qq.com 

---

